const http = require('http');

const requestListener = function (req, res) {
  res.writeHead(200, {
    "Content-Type" : "text/html",
    "Custom-Header": "apapunitu"
  });
  res.end('Hello, <b>Telkom</b> <h1>University</h1>!');
}

const server = http.createServer(requestListener);
server.listen(8080, () => {
  console.log('listening on port 8080')
});

